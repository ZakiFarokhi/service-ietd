const express = require('express');
const app = express();
const bodyParser = require('body-parser');
require('dotenv').config();
const cors = require('cors');
app.use(cors());
app.use(bodyParser.json());

const db = require("./app/config/db.config");
const server = app.listen(3000, function() {
    const host = server.address().address;
    const port = server.address().port;
    console.log(`App running ${host} and port ${port}`)
})

app.get("/", function(req, res){
    res.json({
        success: true
    })
})

db.sequelize.sync({alter:true})

require("./app/router/logUser.router")(app);