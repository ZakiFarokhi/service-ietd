
module.exports = (sequelize, Sequelize) => {
    return sequelize.define('brand',  {
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        user_id: {
            type: Sequelize.STRING(50)
        },
        url: {
            type: Sequelize.STRING(50)
        },
    })
}