const db = require("../config/db.config")
const data = db.logUser;
const {response} = require("../middleware/response");

exports.create = async (req, res) => {
    try {
        const user_id = req.query.user_id;
        const url = req.query.url;

        const Create = await data.create({
            user_id: user_id,
            url : url,
        })
        if(Create){
            response(res, true, "data success", Create)
        } else {
            response(res, false, "cannot create data", null)
        }
    }catch (error){
        response(res, false, "cannot connect db", error)
    }
}