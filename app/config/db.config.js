const Sequelize = require("sequelize");
const sequelize = new Sequelize(
  process.env.DATABASE,
  process.env.DB_USERNAME,
  process.env.DB_PASSWORD,
  {
    host: process.env.DB_HOST,
    dialect: process.env.DB_DIALECT, //configure db Microsoft SQL Server
    operatorsAliases: "0",
    timezone: "+07:00", //indonesia GMT +7
    logging: true,
    //logging: (...msg) => logging(msg), //console.log('tru',msg[0],' bind', JSON.stringify(msg[1].bind)),
    dialectOptions: {
      encrypt: true,
    },
    pool: {
      max: parseInt(process.env.DB_POOL_MAX),
      min: parseInt(process.env.DB_POOL_MIN),
      acquire: parseInt(process.env.DB_POOL_ACQUIRE),
      idle: parseInt(process.env.DB_POOL_IDLE),
    },
  }
);

const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.logUser = require("../model/logUser.model")(sequelize, Sequelize)
module.exports = db;